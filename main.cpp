#include<iostream>
#include<stdlib.h>
#include<math.h>
#include<fstream>
#include<iomanip>

using namespace std;
/**
* Using funtion for Open of files
*/
bool Open(fstream& filename, string name) {
	filename.open(name, ios::in);
	if (!filename.is_open())
		return false;
	else
		return true;
}

/**
* This function find the sum of all array
*/
int SumOfArray(int* Arr, int Size) {
	int sum = 0;
	for (int i = 0; i < Size; i++) {
		sum += Arr[i];
	}
	return sum;
}
/**
* This function find the product of all array
*/
long long int ProductOfArray(int* Arr, int Size) {
	long long Product = 1;
	for (int i = 0; i < Size; i++) {
		Product *= Arr[i];
	}
	return Product;
}
/**
* Finding the avarage of array
*/
float AvarageOfArray(int* Arr, int Size) {
	float Avg = 0.0, Sum = 0.0;
	for (int i = 0; i < Size; i++) {
		Sum += Arr[i];
	}
	Avg = (Sum / Size);
	return Avg;
}

int SmallestOfArray(int* Arr, int Size) {
	int small = INT16_MAX; /** Defualt INT16 Max*/
	for (int i = 0; i < Size; i++) {
		if (small > Arr[i])
			small = Arr[i];
	}
	return small;
}

int main() {
	fstream file;
	int Size = 0, i = 0, input = 0;
	bool True = Open(file, "input.txt");
	if (True == true)
		cout << "The File Opened Succesfully.." << endl;
	else {
		cout << "Error..!" << endl;
		exit(0);
	}
	file >> Size;
	int* Arr = new int[Size]; /** Defining the Dynamic Array Allocation */
	while (true) {
		file >> input;
		if (file.eof()) break;
		Arr[i++] = input;
	}
	cout << fixed << setprecision(1);
	cout << endl << "Sum is " << SumOfArray(Arr, Size);
	cout << endl << "Product is " << ProductOfArray(Arr, Size);
	cout << endl << "Avarage is " << AvarageOfArray(Arr, Size);
	cout << endl << "Smallest is " << SmallestOfArray(Arr, Size);
	cout << endl;
	delete[] Arr; /** Dealocating the Dynamic Array Allocation. */
	return 0;
}